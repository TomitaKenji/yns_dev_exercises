<?php
session_start();

$email='';
$password='';
$username='';
$image_name='';
  if (isset($_POST['submit'])) {
   $email = $_POST['email'];
   $password = $_POST['password'];
   $username = $_POST['username'];
   $image_name = $_FILES['image']['name'];
   $image_tmpname = $_FILES['image']['tmp_name'];
  if (is_uploaded_file($_FILES['image']['tmp_name'])){
    move_uploaded_file( $image_tmpname,"image/$image_name");
    }
  }
   $data = array(
     $email,
     $password,
     $username,
     $image_name
     );

  $err = array();
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $err['email'] = 'invalid email';
   echo $err['email'].'<br />';
 }
  if (!preg_match('/^(?=.*?[a-zA-Z])(?=.*?\d)[a-zA-Z\d]{8,}$/', $password)) {
   $err['password'] = 'Please type using digits and half-width characters in password.';
   echo $err['password'].'<br />';
 }
  if (!preg_match('/^(?=.*?[a-zA-Z])(?=.*?\d)[a-zA-Z\d]{8,}$/', $username)) {
   $err['username'] = 'Please type using digits and half-width characters in username.';
   echo $err['username'].'<br />';
 }
  if (empty($err)) {
   $fp = fopen('user_info.csv', 'a');

  if ($fp){
   fputcsv($fp, $data);
   }
   fclose($fp);
   $_SESSION['me'] = $me;
   header('Location: index.php');
   exit;
  }
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>login</title>
</head>
<body>
 <head>
    <h1>login</h1>
 </head>
 <form  action="index.php" method="post" enctype="multipart/form-data">
      <p>
        <input type="text" name="username" placeholder="username" required>
      </p>
      <p>
        <input type="text" name="email" placeholder="email" required>
      </p>
      <p>
        <input type="password" name="password" placeholder="password" required>
      </p>
      <p>
        <input type="file" name="image" required/>
      </p>
      <input type="submit" name="submit" />
    </form>
</body>
</html>
