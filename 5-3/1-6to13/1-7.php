<?php
if ( isset($_POST['submit'])){
  $email = $_POST['email'];
  $password = $_POST['password'];
  $username = $_POST['username'];
  $data = array(
    $email,
    $password,
    $username
  );
  $err = array();

  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $err['email'] = 'invalid email';
 }
  if (!preg_match('/^(?=.*?[a-zA-Z])(?=.*?\d)[a-zA-Z\d]{8,}$/', $password)) {
   $err['password'] = 'Please type using digits and half-width characters.';
   echo $err['password'];
 }
  if (!preg_match('/^(?=.*?[a-zA-Z])(?=.*?\d)[a-zA-Z\d]{8,}$/', $username)) {
   $err['username'] = 'Please type using digits and half-width characters.';
   echo $err['username'];
 }
  if (empty($err)) {
  echo "correct";
}
}

  ?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>1-6.php</title>
</head>
<body>
 <head>
    <h1>1-7</h1>
 </head>
    <form action="" method="post">
      <p>
        <input type="text" name="username" placeholder="username" required>
      </p>
      <p>
        <input type="text" name="email" placeholder="email" required>
      </p>
      <p>
        <input type="password" name="password" placeholder="password" required>
      </p>
      <input type="submit" name="submit" />
    </form>
</body>
</html>
