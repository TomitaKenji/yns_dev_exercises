<?php
require_once('config.php');
require_once('functions.php');

if (isset($_POST['submit'])) {
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $email = $_POST['email'];
  $password = $_POST['password'];
  $image = $_FILES['image']["name"];

  $dbh = connectDb();

  // error check
  $err = array();
     if (!preg_match('/^[a-zA-Z]+$/', $first_name)) {
        $err['first_name'] = 'Please type using half-width characters.';
     }
     if (!preg_match('/^[a-zA-Z]+$/', $last_name)) {
        $err['last_name'] = 'Please type using half-width characters.';
     }
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $err['email'] = 'Sorry, invalid E-mail.';
     }
     if (emailExists($email, $dbh)) {
        $err['email'] = 'already exist E-mail.';
     }
     if (!preg_match('/^(?=.*?[a-zA-Z])(?=.*?\d)[a-zA-Z\d]{8,}$/', $password)) {
        $err['password'] = 'Please type using digits and half-width characters.';
     }
     if (!preg_match('/\.jpg$|/i', $image)) {
        $err['image'] = 'Please select JPEG　image file.';
     }
     if (empty($err)) {
  // sign up
      $dbh = connectDb();
      $stmt = $dbh->prepare("insert into users( first_name, last_name, email, password, picture_id, created, modified)
      values (:first_name,:last_name,:email,:password,:image, now(), now())");
      $stmt->bindParam(':first_name', $first_name, PDO::PARAM_STR);
      $stmt->bindParam(':last_name', $last_name, PDO::PARAM_STR);
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->bindParam(':password', $password, PDO::PARAM_STR);
      $stmt->bindParam(':image', $image, PDO::PARAM_STR);
      $stmt->execute();
      // //SQLインジェクション
      // $stmt->bindValue('登録するデータ', 変数名, PDO::PARAM_INT);
      // $stmt->execute();
      // var_dump($result);
      header('Location:login.php');
      exit();
    }
  }
// if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
//
//     if (move_uploaded_file($_FILES["image"]["tmp_name"], "images/$image")) {
//         echo $_FILES["image"]["name"] . "をアップロードしました。";
//     }
//     else {
//         echo "ファイルをアップロードできません。";
//     }
// }
// else {
//     echo "ファイルが選択されていません。";
// }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8" />
 <title>sign up</title>
 <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div align="center"><h1>signup</h1></div>
  <form action="" method="post" enctype="multipart/form-data">
    <?php if(isset($err['first_name'])) :?>
    <?php echo '<span style="color:#ff0000;">'. h($err['first_name']).'</span>'; ?>
    <?php endif ;?>
    <p><input type="text" name="first_name" placeholder="FIRST_NAME" required/></p>
    <?php if(isset($err['last_name'])) :?>
    <?php echo '<span style="color:#ff0000;">'. h($err['last_name']).'</span>'; ?>
    <?php endif ;?>
    <p><input type="text" name="last_name" placeholder="LAST_NAME" required/></p>
    <?php if(isset($err['email'])) :?>
    <?php echo '<span style="color:#ff0000;">'. h($err['email']).'</span>'; ?>
    <?php endif ;?>
    <p><input type="text" name="email" placeholder="E-MAIL" required/></p>
    <?php if(isset($err['password'])) :?>
    <?php echo '<span style="color:#ff0000;">'. h($err['password']).'</span>'; ?>
    <?php endif ;?>
    <p><input type="password" name="password" placeholder="PASSWORD" required/></p>
    <p><input type="file" name="image"  value="Picture" /></p>
      <input type="submit" name="submit" class="btn-flat-border" value="signup">
  </form>
</body>
</html>
