<?php
require_once('config.php');
require_once('functions.php');


// function getUser($email, $password, $pdo) {
//     $sql = "select * from users where email = :email and password = :password limit 1";
//     $stmt = $pdo->prepare($sql);
//     $stmt->execute(array(":email"=>$email, ":password"=>$password));
//     $user = $stmt->fetch();
//     return $user ? $user : false;
// }

 if (isset($_POST['submit'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];

  $dbh = connectDb();

  $err = array();
  if (!$me = getUser($email, $password, $dbh)) {
         $err['password'] = 'dont match them';
     }
  if (empty($err)) {
        $_SESSION['me'] = $me;
        header('Location: index.php');
        exit;
     }
}
 ?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>LogIn</title>
  <link rel="stylesheet" href="styles.css">
  <div align="center"><h1>login</h1></div>
</head>
<body>
  <div id="container">
    <form action="" method="post" id="login">
      <?php if(isset($err['email'])) :?>
      <?php echo '<span style="color:#ff0000;">'. h($err['email']).'</span>'; ?>
      <?php endif ;?>
      <p><input type="text" name="email" placeholder="Email" ></p>
      <?php if(isset($err['password'])) :?>
      <?php echo '<span style="color:#ff0000;">'. h($err['password']).'</span>'; ?>
      <?php endif ;?>
      <p><input type="password" name="password" placeholder="PASSWORD"></p>
      <input type="submit" name="submit" class="btn-flat-border" value="login">

      <p class="fs12"><a href="signup.php">Sign Up</a></p>
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
  </div>
</body>
</html>
