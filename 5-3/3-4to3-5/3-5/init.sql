create database 3_5_php;

  grant all on 3_5_php.* to dbuser@localhost identified by 'mu4uJsif';

    use 3_5_php

    create table users(
      id int not null auto_increment primary key,
      first_name varchar(64),
      last_name varchar(64),
      email varchar(255) unique,
      password varchar(255),
      picture_id varchar(255),
      created datetime,
      modified datetime
    );

    desc users;

insert into users( first_name, last_name, email, password, picture_id, created, modified)
  values('kenji','tomita','kenji@sample.com','password1','image0.jpeg', now(), now()),
  ('ryouji','nagai','ryouji@sample.com','password2','image1.jpeg', now(), now()),
  ('sample0','tomita','sample0@sample.com','password3','image2.jpeg', now(), now()),
  ('sample1','tomita','sample1@sample.com','password4','image3.jpeg', now(), now()),
  ('sample2','tomita','sample2@sample.com','password5','image4.jpeg', now(), now()),
  ('sample3','tomita','sample3@sample.com','password6','image5.jpeg', now(), now()),
  ('sample4','tomita','sample4@sample.com','password7','image0.jpeg', now(), now()),
  ('sample5','tomita','sample5@sample.com','password8','image1.jpeg', now(), now()),
  ('sample6','tomita','sample6@sample.com','password9','image2.jpeg', now(), now()),
  ('sample7','tomita','sample7@sample.com','password10','image3.jpeg', now(), now()),
  ('sample8','tomita','sample8@sample.com','password11','image4.jpeg', now(), now()),
  ('sample9','tomita','sample9@sample.com','password12','image5.jpeg', now(), now()),
  ('sample10','tomita','sample10@sample.com','password13','image0.jpeg', now(), now()),
  ('sample11','tomita','sample11@sample.com','password14','image1.jpeg', now(), now()),
  ('sample12','tomita','sample12@sample.com','password15','image2.jpeg', now(), now()),
  ('sample13','tomita','sample13@sample.com','password16','image3.jpeg', now(), now()),
  ('sample14','tomita','sample14@sample.com','password17','image4.jpeg', now(), now());




    -- try {
    --
    --     /* リクエストから得たスーパーグローバル変数をチェックするなどの処理 */
    --
    --     // データベースに接続
    --     $pdo = new PDO(
    --         'mysql:dbname=testdb;host=localhost;charset=utf8mb4',
    --         'root',
    --         '',
    --         [
    --             PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    --             PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    --         ]
    --     );
    --
    --     /* データベースから値を取ってきたり， データを挿入したりする処理 */
    --
    -- } catch (PDOException $e) {
    --
    --     // エラーが発生した場合は「500 Internal Server Error」でテキストとして表示して終了する
    --     // - もし手抜きしたくない場合は普通にHTMLの表示を継続する
    --     // - ここではエラー内容を表示しているが， 実際の商用環境ではログファイルに記録して， Webブラウザには出さないほうが望ましい
    --     header('Content-Type: text/plain; charset=UTF-8', true, 500);
    --     exit($e->getMessage());
    --
    -- }
