<?php

ini_set('display_errors', 1);

define('DSN', 'mysql:dbname=3_5_php;host=localhost;charset=utf8mb4');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('SITE_URL','');
define('IMAGE_DIR','/images');
define('COMMENTS_PER_PAGE', 5);

session_start();
