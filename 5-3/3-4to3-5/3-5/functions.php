<?php

function h($s) {
  return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}

function r($s) {
    return mysql_real_escape_string($s);
}

function connectDb() {
    try {
        return new PDO(DSN, DB_USER, DB_PASSWORD,[PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,]);
    } catch (PDOException $e) {
        echo $e->getMessage();
        exit;
    }
}

function emailExists($email, $dbh) {
    $sql = "select * from users where email = :email limit 1";
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array(":email" => $email));
    $user = $stmt->fetch();
    return $user ? true : false;
}

function getUser($email, $password, $dbh) {
    $sql = "select * from users where email = :email and password = :password limit 1";
    $stmt = $dbh->prepare($sql);
    $stmt->execute(array(":email"=>$email, ":password"=>$password));
    $user = $stmt->fetch();
    return $user ? $user : false;
}

// function checkExistingEmail($email) {
//   try {
//      $pdo = new PDO(
//        'mysql:dbname=3_5_php;host=localhost;charset=utf8mb4',
//        'dbuser',
//        'mu4uJsif',
//        [
//            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//        ]
//        );
//     $query = $pdo->prepare("select * from users where email = :email limit 1");
//     $stmt->bindParam(':email', $email, PDO::PARAM_STR);
//     $stmt->excute();
//     } catch (PDOException $e) {
//       exit($e->getMessage());
//     }
//   }
