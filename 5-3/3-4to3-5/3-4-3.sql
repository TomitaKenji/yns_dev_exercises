SELECT
    CONCAT(first_name, " ", last_name) AS full_name, hire_date
FROM
    employees
WHERE
    hire_date >= "2015-1-1" AND hire_date <= "2015-3-31"
ORDER BY
    hire_date ASC
