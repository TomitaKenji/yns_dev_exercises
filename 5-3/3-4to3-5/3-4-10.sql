SELECT
    emp.first_name,
    emp.middle_name,
    emp.last_name
FROM
    employees AS emp
RIGHT JOIN(
    SELECT pos.employee_id
    FROM
        employee_positions AS pos
    GROUP BY
        pos.employee_id
    HAVING
        COUNT(pos.position_id) > 1
) AS position_table
ON
    emp.id = position_table.employee_id
