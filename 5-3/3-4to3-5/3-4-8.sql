SELECT
    first_name,
    middle_name,
    last_name,
    hire_date
FROM
    employees
ORDER BY
    hire_date ASC
LIMIT 1
