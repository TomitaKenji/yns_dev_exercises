SELECT
    last_name
FROM
    employees
WHERE
    department_id =(
    SELECT
        id
    FROM
        departments
    WHERE NAME
        = "Sales"
)
ORDER BY
    last_name
DESC;
    
