
drop table if exists employees;
create table employees (
  id int,
  first_name varchar(255),
  last_name varchar(255) default null,
  middle_name varchar(255) default null,
  birth_date date,
  department_id int,
  hire_date date default null,
  boss_id int
);
desc employees;

insert into employees ( id, first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id)
  values (1,'Manabu', 'Yamazaki',	null,'1976-03-15',	1,	null,	null),
  (2, 'Tomohiko', 'Takasago', null,'1974-05-24',	3,'2014-04-01',	1),
  (3, 'Yuta', 'Kawakami', null,'1990-08-13', 4,'2014-04-01',	1),
  (4, 'Shogo', 'Kubota', null,'1985-01-31',	4,'2014-12-01',	1),
  (5, 'Lorraine',	'San Jose',	'P.','1983-10-11',	2,'2015-03-10',	1),
  (6, 'Haille', 'Dela Cruz',	'A.','1990-11-12',	3,'2015-02-15',	2),
  (7,	'Godfrey',	'Sarmenta',	'L.','1993-09-13',	4,'2015-01-01',	1),
  (8,	'Alex',	'Amistad', 'F.','1988-04-14',	4,'2015-04-10',	1),
  (9,	'Hideshi',	'Ogoshi',	null,'1983-07-15',	4,'2014-06-01',	1),
  (10,	'Kim',	null, null,'1977-10-16',	5,'2015-08-06',	1);

drop table if exists departments;
create table departments (
  id int,
  name varchar(255)
);

desc departments;

insert into departments ( id, name)
  values (1,'Exective'),
  (2,'Admin'),
  (3,'Sales'),
  (4,'Development'),
  (5,'Desgin'),
  (6,'Marketing');

  drop table if exists positions;
  create table positions (
    id int primary key auto_increment,
    name varchar(255)
  );

  desc positions;

  insert into positions ( id, name)
    values(1, 'CEO'),
    (2, 'CTO'),
    (3, 'CFO'),
    (4, 'Manager'),
    (5, 'Staff');

     drop table if exists employee_positions;
    create table employee_positions (
      id int unsigned primary key auto_increment,
      employee_id int,
      position_id int
    );

    desc employee_positions;

    insert into employee_positions ( id, employee_id, position_id)
      values ( 1,1,1),
      (2,1,2),
      (3,1,3),
      (4,2,4),
      (5,3,5),
      (6,4,5),
      (7,5,5),
      (8,6,5),
      (9,7,5),
      (10,8,5),
      (11,9,5),
      (12,10,5);



  -- select * from employees;
  -- select * from departments;
  -- select * from positions;
  -- select * from employee_positions;

  select * from employees where last_name like 'K%';
  select * from employees where last_name like '%I';
  select first_name, middle_name, last_name, hire_date from employees where hire_date between '2015-01-01' and '2015-03-31' order by hire_date asc;
  select last_name, boss_id from employees where boss_id = 1;
  select last_name from employees where department_id = 3 order by last_name desc;
  select count(middle_name) from employees;
  SELECT name,COUNT(departments.name)
  FROM departments
  JOIN employees
  ON departments.id=employees.department_id
  GROUP BY departments.name
  select first_name, middle_name, last_name, max(hire_date) from employees;
  select * from departments left outer join employees on employees.department_id = departments.id where employees.department_id is null;
  SELECT COUNT(*),last_name FROM employee_position JOIN employees ON employee_position.employee_id = employees.id GROUP BY employee_id HAVING COUNT(*)>2;
