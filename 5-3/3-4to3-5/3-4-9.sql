SELECT
    UPPER(dept.name)
FROM
    departments AS dept
WHERE NOT EXISTS
    (
    SELECT
        department_id
    FROM
        employees AS emp
    WHERE
        emp.department_id = dept.id
);
