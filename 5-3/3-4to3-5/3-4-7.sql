SELECT
    UPPER(dept.name) AS name,
    COUNT(emp.department_id) AS count_per_department
FROM
    departments AS dept
RIGHT JOIN employees AS emp
ON
    dept.id = emp.department_id
GROUP BY
    dept.name
ORDER BY
    dept.id ASC;
