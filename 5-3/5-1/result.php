<?php
SESSION_start();

function h($s) {
  return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}
if (isset($_POST['submit'])){
  $_SESSION['correct_count']= 0;
  $_SESSION['num']= 0;
  header('Location: quiz.php');
}
 ?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Quiz</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div style="padding:7px;background:#eee;border:#ccc;">
      10 問中
      <?php echo h($_SESSION['correct_count']); ?> 問正解！
      <?php if ($_SESSION['num'] > 0) : ?>
        正答率は <?php echo h( sprintf("%.2f", $_SESSION['correct_count'] / 10 * 100) ); ?> % です！
      <?php endif; ?>
      <form action="" method="post">
        <input type="submit" name="submit" value="logout"/>
      </form>
  </div>
</body>
</html>
