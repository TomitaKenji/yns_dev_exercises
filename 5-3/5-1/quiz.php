<?php


 SESSION_start();

 function h($s) {
   return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
 }
   define('DB_DATABASE', 'multiple');
   define('DB_USERNAME', 'root');
   define('DB_PASSWORD', 'root');
   define('PDO_DSN', 'mysql:host=localhost;dbname=' . DB_DATABASE);

   try {
     // connect
     $db = new PDO(PDO_DSN, DB_USERNAME, DB_PASSWORD);
     $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   } catch (PDOException $e) {
     echo $e->getMessage();
     exit;
   }
    $stmt = $db->query("select * from quizList");
    $quizlists = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $quizList=[];
    $i= 0;

     //データベースから配列を作成
     foreach ($quizlists as $quizlist) {
     // var_dump($quizlist[1]);
       $quizList[] = [
         'q' =>$quizlist['Question'],
         'a' => [$quizlist['choice0'], $quizlist['choice1'], $quizlist['choice2']]
       ];
       $i++;
     }
    function resetSession() {
     $_SESSION['correct_count'] = 0;
     $_SESSION['num'] = 0;
     }

    function redirect() {
     header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
     exit;
     }

     if ($_SERVER['REQUEST_METHOD'] === 'POST') {
     if (isset($_POST['reset']) && $_POST['reset'] === '1') {
          resetSession();
          redirect();
        }

     if ($_POST['answer'] === $quizList[$_POST['qnum']]['a'][0]) {
        $_SESSION['correct_count']++;
         }
         $_SESSION['num']++;
         redirect();
         }

     if ($_SESSION['num'] > 9){
        $_SESSION['num'] = 0 ;
        header('Location: result.php');
     }
     if (empty($_SESSION)) {
         $_SESSION['correct_count'] = 0;
         $_SESSION['num'] = 0;
         }


    $qnum = mt_rand(0, $i - 1);
    $quiz = $quizList[$qnum];

    $_SESSION['qnum'] = (string)$qnum;
    shuffle($quiz['a']);

   ?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Quiz</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div style="padding:7px;background:#eee;border:#ccc;">
      <?php echo h($_SESSION['num']); ?> 問中
      <?php echo h($_SESSION['correct_count']); ?> 問正解！
      <?php if ($_SESSION['num'] > 0) : ?>
        正答率は <?php echo h( sprintf("%.2f", $_SESSION['correct_count'] / $_SESSION['num'] * 100) ); ?> % です！
      <?php endif; ?>
  </div>
  <p>Q. <?php echo h($quiz['q']); ?></p>
    <?php foreach ($quiz['a'] as $answer) : ?>
        <form action="" method="post">
            <input type="submit" name="answer" value="<?php echo h($answer); ?>">
            <input type="hidden" name="qnum" value="<?php echo h($_SESSION['qnum']); ?>">
        </form>
    <?php endforeach; ?>

    <hr>
   <form action="" method="post">
       <input type="submit" value="リセット">
       <input type="hidden" name="reset" value="1">
   </form>

</body>
</html>
