<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/nav.css">
</head>
<body>
    <h1 class="title">Navigation</h1>

    <div class="vertical-menu">
        <a href="/php/yns_dev_exercises/5-3/1-6/1-1.php">[1-1]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6/1-2.php">[1-2]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6/1-3.php">[1-3]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6/1-4.php">[1-4]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6/1-5.php">[1-5]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6/1-5.php">[1-6]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-7.php">[1-7]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-8.php">[1-8]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-9.php">[1-9]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-10.php">[1-10]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-11.php">[1-11]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-12.php">[1-12]</a>
        <a href="/php/yns_dev_exercises/5-3/1-6to13/1-13/index.php">[1-13]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-1.html">[2-1]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-2.html">[2-2]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-3.html">[2-3]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-4.html">[2-4]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-5.html">[2-5]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-6.html">[2-6]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-7.html">[2-7]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-8.html">[2-8]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-9.html">[2-9]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-10.html">[2-10]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-11.html">[2-11]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-12.html">[2-12]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-13.html">[2-13]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-14.html">[2-14]</a>
        <a href="/php/yns_dev_exercises/5-3/2-1to15%20/2-15.html">[2-15]</a>
        <a href="/php/yns_dev_exercises/5-3/3-4to3-5/3-5/login.php">[3-5]</a>
        <a href="/php/yns_dev_exercises/5-3/5-1/quiz.php">[5-1]</a>
        <a href="/php/yns_dev_exercises/5-3/5-2/calender.php">[5-2]</a>
        <a href="5-3.css">[5-3]</a>
    </div>
</body>
</html>
