<!DOCTYPE html>
<html>
<head>
    <title>1-3</title>
</head>
<body>

    <form action='1-3.php' method='post'>
        <input type='number' name='first_value'>
        <input type='number' name='second_value'>
        <input type='submit' name='gcd' value='go'>
    </form>

<?php
$value = 0;

if (isset($_POST['gcd'])) {
    $temp = 0;
    $value_first = $_POST['first_value'];
    $value_second = $_POST['second_value'];
    define('INFINITE', 1);
    while ('INFINITE') {
        if ($value_second == 0) {
            $value = $value_first;
            break;
        }
    $temp = $value_second;
    $value_second = $value_first % $value_second;
    $value_first = $temp;
    }
}
echo $value;
?>
</body>
</html>
