<!DOCTYPE html>
<html>
<head>
    <title>1-5</title>
</head>
<body>
    <form action="1-5.php" method="post">
        <input type="date" name="date_value">
        <input type="submit" name="go">
    </form>

    <?php

        if (isset($_POST['go'])) {
            echo '<p>Original date: '.$_POST['date_value'].'</p>';
            define('MAX_NUMBER_CAN_BE_ADD', 3);
            for ($day = 1; $day <= MAX_NUMBER_CAN_BE_ADD; $day++) {
                $date = date('Y-m-d', strtotime($_POST['date_value']. ' + '.$day.' days'));
                $day_of_the_week = date('l', strtotime($date));
                echo '<p>'.$date.': '.$day_of_the_week.'</p>';
            }
        }
    ?>
</body>
</html>
