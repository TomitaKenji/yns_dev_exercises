<!DOCTYPE html>
<html>
<head>
    <title>1-4</title>
</head>
<body>
    <form action='' method='post'>
        <input type='number' name='first_value'>
        <input type='submit' name='go'>
    </form>

    <?php
    if (isset($_POST['go'])) {
        $first_value = $_POST['first_value'];
        for ($i = 1; $i <= $first_value ; $i++) {
            if ($i % 3 === 0 && $i % 5 === 0) {
                echo 'FiZZBUZZ ';
            } else if (!($i % 3 === 0) && !($i % 5 === 0)) {
                echo $i .' ';
            } else if ($i % 3 === 0) {
                echo 'FIZZ ';
            } else if ($i % 5 === 0) {
                echo 'BUZZ ';
            }
        }
    }
    ?>
</body>
</html>
